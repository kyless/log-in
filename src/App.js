import React, { Component } from 'react';
import AmazonLogo from './Amazon-logo.png'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="Header">
          <a href="https://www.amazon.com/">
            <img src={AmazonLogo} title="AmazonLogo" alt="AmazonLogo"/>
          </a>
        </div>
      <div className="Box">
        <div className="SignInText">
          <h1>Sign in</h1>
          <strong><p>Email (phone for mobile account)</p></strong>
          <input type="text"/>
          <button text="Hello"/>
        </div>

        </div>
      </div>
    );
  }
}

export default App;
